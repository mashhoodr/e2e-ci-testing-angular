import { E2eTestPage } from './app.po';

describe('e2e-test App', () => {
  let page: E2eTestPage;

  beforeEach(() => {
    page = new E2eTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
